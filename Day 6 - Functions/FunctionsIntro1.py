# Functions with Input
# def function_with_name(name, age):

#  print(f"Hello, your name is {name} and you are {age} years old")
# function_with_name(name=input("Enter your Name here sir/ma !!!"))
def greet_with(name, location):
    print(f"Hello {name}")
    print(f"What is it like in {location}")

greet_with(location="Canada", name="Adepentane")
