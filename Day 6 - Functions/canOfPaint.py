import math
import random


"""
number of cans = (wall height ✖️ wall width) ÷ coverage per can.
e.g. Height = 2, Width = 4, Coverage = 5
number of cans = (2 ✖️ 4) ÷ 5
"""
#Write your code below this line 👇

def paint_calc(height, width, cover):
    area = height * width
    num_of_cans = math.ceil(area/cover)
    print(f"You'll need {num_of_cans} cans of Paint to finish this work")

test_h = int(input("Height of wall: "))
test_w = int(input("Width of wall: "))
coverage = 5
paint_calc(height=test_h, width=test_w, cover=coverage)


