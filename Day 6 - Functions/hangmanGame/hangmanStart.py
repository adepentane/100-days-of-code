import random

# Step 1
# Build a list
word_list = ["ardvark", "baboon", "camel"]

# TODO1 - Randomly selects a word from the list of words
# to start guessing.
# TODO 2 = Ask the User to guess a letter and assign their
# answer to a variable guess. make guess lowercase

# TODO 3 = check to see if the letter guessed is part of
# the alphabets in the Letter generated randomly.


chosen_word = random.choice(word_list)

guess = input("Guess a letter: ").lower()
for letter in chosen_word:
    if letter == guess:
        print("Right")
    else:
        print("Wrong")
