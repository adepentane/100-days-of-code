#This is the While Loop lessons.
# In the While Loop, the code keeps running as long as the
# conditions are being met.

number_of_hurdles = 6
while number_of_hurdles > 0:
    number_of_hurdles -= 1
    print(number_of_hurdles)